package com.good;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class IncrementCalculatorTest {
    EmployeeDetails e = new EmployeeDetails();
    EmployeeDetails e1 = new EmployeeDetails();
    EmployeeDetails e2 = new EmployeeDetails();
    IncrementCalculator i = new IncrementCalculator();

    @Test
    @DisplayName("Testing of calculateIncrement method in IncrementCalculator class")
    void calculateIncrementTest() {
        e.setUserExperience(0.5f);
        e.setSalary(10000);
        e1.setUserExperience(1.5f);
        e1.setSalary(10000);
        e2.setUserExperience(3.5f);
        e2.setSalary(10000);
        Assertions.assertAll(
                () -> Assertions.assertEquals(1000, i.calculateIncrement(e)),
                () -> Assertions.assertEquals(2000, i.calculateIncrement(e1)),
                () -> Assertions.assertEquals(3000, i.calculateIncrement(e2))
        );

    }
}