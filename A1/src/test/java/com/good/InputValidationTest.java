package com.good;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class InputValidationTest {
    EmployeeDetails e = new EmployeeDetails();
    EmployeeDetails e1 = new EmployeeDetails();
    EmployeeDetails e2 = new EmployeeDetails();
    InputValidation i = new InputValidation();

    @Test
    @DisplayName("Testing of validInput method in InputValidation class")
    void validInput() {
        e.setUserExperience(-1);
        e1.setUserExperience(101);
        e2.setSalary(-10);

        Assertions.assertAll(
                () -> Assertions.assertFalse(i.validInput(e)),
                () -> Assertions.assertFalse(i.validInput(e1)),
                () -> Assertions.assertFalse(i.validInput(e2))
        );
    }
}