package com.good;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class EmployeeDetailsTest {

    EmployeeDetails e = new EmployeeDetails();

    @Test
    @DisplayName("Testing isExceptionOccurred method in EmployeeDetails class")
    void isExceptionOccurredTest() {
        e.setExceptionOccurred(true);
        Assertions.assertTrue(e.isExceptionOccurred());
        e.setExceptionOccurred(false);
        Assertions.assertFalse(e.isExceptionOccurred());

    }

    @Test
    @DisplayName("Testing getEmployeeDetail method in EmployeeDetails class ")
    void getEmployeeDetailTest() {

        Assertions.assertDoesNotThrow(() -> e.setUserExperience(2.5f));
        Assertions.assertDoesNotThrow(() -> e.setSalary(10000));

        Assertions.assertEquals(2.5, e.getUserExperience());
        Assertions.assertEquals(10000, e.getSalary());

    }
}