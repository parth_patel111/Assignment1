package com.good;

public class IncrementCalculatorNewPolicy implements ICalculation {

    @Override
    public int calculateIncrement(EmployeeDetails e) {
        float userExperience = e.getUserExperience();

        int empSalary = e.getSalary();
        int increment;

        if (userExperience <= 1) {
            increment = (int) (empSalary * 0f);
        } else if (userExperience <= 2) {
            increment = (int) (empSalary * 0.1f);
        } else {
            increment = (int) (empSalary * 0.2f);
        }
        return increment;
    }
}
