package com.good;

public class IncrementCalculator implements ICalculation {

    @Override
    public int calculateIncrement(EmployeeDetails e) {
        float userExperience = e.getUserExperience();

        int empSalary = e.getSalary();
        int increment;

        if (userExperience <= 1) {
            increment = (int) (empSalary * 0.1);
        } else if (userExperience <= 2) {
            increment = (int) (empSalary * 0.2);
        } else {
            increment = (int) (empSalary * 0.3);
        }
        return increment;
    }
}
