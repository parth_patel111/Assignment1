package com.good;

public class Main {
    public static void main(String[] args) {

        EmployeeDetails e = new EmployeeDetails();
        IValidate in = new InputValidation();

        e.getEmployeeDetail();

        if (in.validInput(e) && !e.isExceptionOccurred()) {
            ICalculation i = new IncrementCalculator();
            //When company will introduce new calculation policy then we can reflect the changes in the code by this way
            //ICalculation i = new IncrementCalculatorNewPolicy();
            System.out.println("Increment of an employee is :" + i.calculateIncrement(e));
            System.out.println("New salary after increment is: " + (e.getSalary() + i.calculateIncrement(e)));
        } else {
            System.out.println("System cannot calculate increment due to invalid input");
        }
    }
}
