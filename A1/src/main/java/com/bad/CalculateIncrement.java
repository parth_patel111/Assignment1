package com.bad;

public class CalculateIncrement {
    public float calculateIncrement(EmpDetails e) {

        float userExperience = e.getUserExperience();
        int empSalary = e.getSalary();

        if (userExperience <= 12) {
            return empSalary * 0.1f;
        } else if (userExperience <= 24) {
            return empSalary * 0.2f;
        } else {
            return empSalary * 0.3f;
        }
    }
}
