package com.bad;

public class Main {
    public static void main(String[] args) {
        EmpDetails empDetails = new EmpDetails();
        InputValidation inputValidation = new InputValidation();
        empDetails.getEmployeeDetail();

        if (inputValidation.validInput(empDetails) && !empDetails.isExceptionOccurred()) {
            CalculateIncrement calculateIncrement = new CalculateIncrement();
            System.out.println("Increment of an employee is :" + calculateIncrement.calculateIncrement(empDetails));
            System.out.println("new salary after increment is: " + (empDetails.getSalary() + calculateIncrement.calculateIncrement(empDetails)));
        } else {
            System.out.println("System cannot calculate increment due to invalid input");
        }
    }
}
