package com.bad;

import java.util.InputMismatchException;
import java.util.Scanner;

public class EmpDetails {

    private int salary;
    private float userExperience;
    private boolean exceptionOccurred;

    Scanner sc = new Scanner(System.in);

    public boolean isExceptionOccurred() {
        return exceptionOccurred;
    }

    public void setExceptionOccurred(boolean exceptionOccurred) {
        this.exceptionOccurred = exceptionOccurred;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public float getUserExperience() {
        return userExperience;
    }

    public void setUserExperience(float userExperience) {
        this.userExperience = userExperience;
    }

    public void getEmployeeDetail() {
        try {
            System.out.println("enter the experience of employee in year:");
            setUserExperience(sc.nextFloat());
            System.out.println("enter current Employee Salary:");
            setSalary(sc.nextInt());
        } catch (InputMismatchException i) {
            setExceptionOccurred(true);
            System.out.println("invalid input, please enter a valid number");
        } finally {
            sc.close();
        }
    }
}