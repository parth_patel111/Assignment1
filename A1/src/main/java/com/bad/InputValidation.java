package com.bad;

public class InputValidation {
    public boolean validInput(EmpDetails e) {
        if (e.getUserExperience() < 0 || e.getUserExperience() > 100 || e.getSalary() < 0) {
            System.out.println("Please enter valid value");
            return false;
        }
        return true;
    }
}
